﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMovement : MonoBehaviour
{
    public float moveSpeed = 20.0f;
    private Vector3 pos;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pos = transform.position; // recebendo os três eixos
        pos.x += moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
        pos.z = +moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
        transform.position = pos;
    }
}
